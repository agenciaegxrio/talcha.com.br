TALCHA :)
====

## Pre-requisites

* Node - http://nodejs.org/
* Git - http://git-scm.com/

After installing node, use `npm` to install the Grunt command line application and Bower.

    npm install -g grunt-cli

    npm install -g bower
    
If you are using Windows, remember to put `git` on your PATH or use the git bash included in installation.

## Install

Clone this repo or download and unzip it.

## Quick Start

Enter the project folder you cloned or downloaded, install dependencies and run `grunt`:

    npm install
    bower install
    grunt 

## Publish
  
    #Generate compress files

    grunt
  
    # Upload this files to vtex:

    # public/arquivos/talcha-styles.min.css
    # public/arquivos/talcha-javascripts.min.js

DEVELOPMENT MODE
====

## RUN LOCALLY
  
    node server.js

    # Open localhost:3000 in your browser

Before you run project locally, copy source code of pages and create html files in public folder.

Home Page > index.html
Category Page > category.html
Product Page > product.html

If you need more pages, add other routes in server.js:

    app.get('/page-name', function(req, res){
      res.render('page-file.html');
    })

    

## JS MAP

Open requirejs task file in folder build/tasks, and set generateSourceMaps to true.

Don't publish source maps files in production.

## BOOTSTRAP

Selecionei apenas algumas funções do Bootstrap que se encontra em bootstrap/config.json

## Watch files
  
    grunt watch
