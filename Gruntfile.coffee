module.exports = ->
 
  @loadTasks "build/tasks"

  @registerTask "default", [
    "clean"
    "stylus"
    # "requirejs"
    "cssmin"
    "uglify"
    "copy"
  ]

  @registerTask "css-all", [
    "stylus"
    "cssmin"
    "copy"
  ]