var express = require('express')
var app = express()

// App Configurations

app.use(express.static(__dirname + '/public'));

app.set('views', __dirname + '/public');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

// Routes
app.get('/', function (req, res) {
});

app.get('/category', function(req, res){
  res.render('category.html');
});

app.get('/department', function(req, res){
  res.render('department.html');
});

app.get('/product', function(req, res){
  res.render('product.html');
});

app.get('/login', function(req, res){
  res.render('login.html');
});

app.get('/institucional/empresa', function(req, res){
  res.render('institucional-empresa.html');
});

app.get('/institucional/lojas', function(req, res){
  res.render('institucional-lojas.html');
});

app.get('/institucional/formas-de-pagamento', function(req, res){
  res.render('institucional-formas-de-pagamento.html');
});

app.get('/institucional/politica-de-privacidade', function(req, res){
  res.render('institucional-politica-de-privacidade.html');
});

app.get('/institucional/politica-de-seguranca', function(req, res){
  res.render('institucional-politica-de-seguranca.html');
});

app.get('/novo-no-cha', function(req, res){
  res.render('novo-no-cha.html');
});

app.get('/novo-no-cha/tipos-de-chas', function(req, res){
  res.render('novo-no-cha-Tipos-de-chas.html');
});

app.get('/novo-no-cha/preparando-meu-cha', function(req, res){
  res.render('novo-no-cha-Preparando-meu-cha.html');
});

app.get('/novo-no-cha/a-historia-dos-chas', function(req, res){
  res.render('novo-no-cha-A-historia-dos-chas.html');
});

app.get('/novo-no-cha/dicas', function(req, res){
  res.render('novo-no-cha-Dicas.html');
});

app.get('/contact', function(req, res){
  res.render('contact.html');
});

app.get('/account', function(req, res){
  res.render('account.html');
});

var server = app.listen(4001, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('App listening at http://%s:%s', host, port)

})

// live reload
// <script src="//localhost:35729/livereload.js"></script>