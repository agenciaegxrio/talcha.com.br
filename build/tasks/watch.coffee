module.exports = ->
  @loadNpmTasks "grunt-contrib-watch"

  # Minify the distribution CSS.
  @config "watch",
    options:
      livereload: true
    css:
      files: ['assets/stylesheets/**/*.styl', 'assets/stylesheets/**/*.css']
      tasks: ['css-all']
    js:
      files: ['assets/javascripts/**/*.js']
      tasks: ['default']
    html:
      files: ['public/*.html']
      # tasks: ['css-all']