module.exports = ->
  @loadNpmTasks "grunt-contrib-copy"

  shop = "talcha"

  @config "copy",
    css:
      files: [
        src: "dist/styles.min.css"
        dest: "public/arquivos/#{shop}-styles.min.css"
      ]
    js:
      files: [
        src: "dist/javascripts.min.js"
        dest: "public/arquivos/#{shop}-javascripts.min.js"
      ]
    js_map:
      files: [
        src: "dist/javascripts.min.js.map"
        dest: "public/arquivos/javascripts.min.js.map"
      ]
    # sprite:
    #   files: [
    #     src: "assets/images/sprite.png",
    #     dest: "dist/#{shop}-sprite.png"
    #   ]
    images:
      files: [
        expand: true,
        cwd: 'assets/images/',
        src: '**',
        dest: 'public/arquivos/',
        flatten: true
      ]
    fonts:
      files: [
        expand: true,
        cwd: 'assets/fonts/',
        src: '**',
        dest: 'public/arquivos/',
        flatten: true
      ]