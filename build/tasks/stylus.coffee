module.exports = ->
  @loadNpmTasks "grunt-contrib-stylus"

  @config "stylus",

    compile:
      options:
        compress: true
        linenos: false
        'include css': true
      files:
        'temp/styles.css': 'assets/stylesheets/index.styl'

    # quickView:
    #   options:
    #     compress: true
    #     linenos: false
    #     'include css': true
    #   files:
    #     'public/arquivos/quickView.css': 'assets/stylesheets/product-quick-view.styl'

