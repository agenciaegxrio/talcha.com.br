<header>
<link href="/arquivos/prateleira.css" rel="stylesheet" type="text/css" />
    <div class="row-fluid">
        <div class="span3">
	        <a href="/" title="STORE Totem" class="logo"><img src="/arquivos/vtex_store.jpg" alt="logo"/></a>
		</div>

		<div class="span9">
            <a title="Carrinho de Compras" href="/checkout/" class="btn btn-info pull-right">
                <i class="icon-shopping-cart"></i>
                Meu Carrinho
            </a>

            <div class="welcome pull-right">
                <vtex.cmc:welcomeMessage/>
            </div>
            <div class="search pull-right">
		        <vtex.cmc:fullTextSearchBox/>
            </div>
        </div>        
	</div>

	<div class="navbar">
		<div class="navbar-inner">
	        <vtex.cmc:departmentLinks/>
		</div>
	</div>    
</header>
<br />