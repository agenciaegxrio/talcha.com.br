<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:vtex="http://www.vtex.com.br/2009/vtex-common" xmlns:vtex.cmc="http://www.vtex.com.br/2009/vtex-commerce" lang="pt-br">
<head>
    <vtex:metaTags/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <vtex:template id="commonScripts" />
</head>
<body>
            
    <!--  SubTemplate: Header -->
    <vtex:template id="Header" />
    <!-- /SubTemplate: Header -->
                
    <section id="department">
        <div class="topBanners">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!--  placeholder -->
                        <vtex:contentPlaceHolder id="Banner-Principal" />
                        <!-- /placeholder -->
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="content" class="row">
                <div class="col-sm-2 sidebar">
                    <div id="departmentNavigator">
                        <!--  Controle: navegador de departamento -->
                        <vtex.cmc:singleDepartmentNavigator />
                        <!-- /Controle: navegador de departamento -->
                    </div>                                      
                </div>

                <div class="col-sm-10">
                    <div class="departmentDescription">
                        <!--  placeholder -->
                        <vtex:contentPlaceHolder id="Department-Description" />
                        <!-- /placeholder -->
                    </div>
                    <!-- <div class="title-category">
                        <vtex.cmc:searchTitle />
                    </div> -->
                
                    <div id="collections">
                        <div class="giftlist-insert">
                            <vtex.cmc:GiftListInsertSkuV2 />
                        </div>
                        
                        <div class="collectionWrap">
                            <!--  Controle: Resultado da busca / Lista de produtos -->
                            <vtex.cmc:searchResult layout="ef3fcb99-de72-4251-aa57-71fe5b6e149f" itemCount="12" columnCount="3" />
                            <!--  /Controle: Resultado da busca / Lista de produtos -->
                        </div>
                        <!-- <div class="collectionWrap">
                            <vtex:contentPlaceHolder id="Destaques" />
                        </div>-->
                        
                        <!-- <div class="collectionWrap">
                            <vtex:contentPlaceHolder id="Lançamentos" />   
                        </div> -->
                    </div>
                </div>                
            </div>
        </div>
    </section>

    <!--  SubTemplate: Footer -->
    <vtex:template id="Footer" />
    <!-- /SubTemplate: Footer -->
    <script src="/arquivos/talcha-javascripts.min.js"></script>
</body>
</html>