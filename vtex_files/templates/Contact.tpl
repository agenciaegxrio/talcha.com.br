<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:vtex="http://www.vtex.com.br/2009/vtex-common" xmlns:vtex.cmc="http://www.vtex.com.br/2009/vtex-commerce" lang="pt-br">

<head>
    <title>Talchá</title>
    <vtex:metaTags/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <vtex:template id="commonScripts" />
</head>
<body class="contact">
            
    <!--  SubTemplate: Header -->
    <vtex:template id="Header" />
    <!-- /SubTemplate: Header -->
                
    <section id="contact">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Início</a></li>
                <li class="active">Central de Atendimento</li>
            </ol>
            <div class="row">
                <div class="col-md-2 no-pd-r">
                    <ul class="page-nav nav">
                        <li><a href="/institucional/empresa">Central de Atendimento</a></li>
                        <li><a href="/institucional/duvidas-frequentes">Dúvidas Frequentes</a></li>
                        <li><a href="/institucional/trocas-e-devolucoes">Trocas e Devoluções</a></li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Central de Atendimento</h1>
                        </div>
                        <div class="col-md-4 col-esq">
                            <p><strong>Por telefone:</strong></p>
                            <p><strong>SAC Talchá</strong></p>
                            <p>Tel: (11) 3078-4945</p>
                            <p>Horário de atendimento:</p>
                            <p>Dias Úteis<br> 9h às 18h</p>
                            <p><strong>Shopping Pátio Higienópolis</strong></p>
                            <p>Tel: +55 11 3823 3744</p>
                            <p><strong>Shopping JK Iguatemi</strong></p>
                            <p>Tel: +55 11 3152 6744</p>
                            <p><strong>Shopping Paulista</strong></p>
                            <p>Tel: +55 11 3152 6744</p>
                            <p>Horário de atendimento:</p>
                            <p><strong></strong>Todos os dias<br>10h às 22h</p>
                        </div>
                        <div class="col-md-8">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--  SubTemplate: Footer -->
    <vtex:template id="Footer" />
    <!-- /SubTemplate: Footer -->
    <script src="/arquivos/talcha-javascripts.min.js"></script>
</body>
</html>