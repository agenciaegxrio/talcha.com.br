<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a class="footer-logo sprite" href="/" title="Talchá">Talchá</a>    
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="row menu-links">
                    <div class="col-md-3">
                        <ul class="menu-itens item-1">
                            <li class="parent"><a href="#" title="Institucional" rel="nofollow">Institucional</a></li>

                            <li class="child"><a href="/institucional/empresa" title="A Empresa" rel="nofollow">A Empresa</a></li>

                            <li class="child"><a href="/institucional/lojas" title="Nossa Loja" rel="nofollow">Nossa Loja</a></li>

                            <li class="child"><a href="/central-de-atendimento" title="Fale Conosco" rel="nofollow">Fale Conosco</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu-itens item-2">
                            <li class="parent"><a href="#" title="Segurança" rel="nofollow">Segurança</a></li>

                            <li class="child"><a href="/institucional/politica-de-privacidade" title="Política de Privacidade" rel="nofollow">Política de Privacidade</a></li>

                            <li class="child"><a href="/institucional/politica-de-seguranca" title="Política de Segurança" rel="nofollow">Política de Segurança</a></li>

                            <li class="child"><a href="/institucional/formas-de-pagamento" title="Formas de Pagamento" rel="nofollow">Formas de Pagamento</a></li>

                            <li class="child"><a href="/institucional/prazos-e-custos-de-entrega" title="Prazos de Entrega" rel="nofollow">Prazos de Entrega</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu-itens item-3">
                            <li class="parent"><a href="#" title="Ajuda e Suporte" rel="nofollow">Ajuda e Suporte</a></li>

                            <li class="child"><a href="/institucional/duvidas-frequentes" title="Dúvidas Frequentes" rel="nofollow">Dúvidas Frequentes</a></li>

                            <li class="child"><a href="/institucional/trocas-e-devolucoes" title="Trocas e Devoluções" rel="nofollow">Trocas e Devoluções</a></li>

                            <li class="child"><a href="#" title="Esqueci minha senha" rel="nofollow">Esqueci minha senha</a></li>
                        </ul>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-12 pagamentos-seguranca">
                        <div class="row">
                            <div class="menu-links">
                                <ul class="menu-itens item-1">
                                    <li class="child">


                                        <div class="banner-box">
                                            <a class="banner-imagem" href="/institucional/formas-de-pagamento" title="Formas de Pagamento" target="_self"><img src="http://www.talcha.com.br/media/custom/banners/resize/rodape_pagamento/pagto-1426017783.png" width="260" height="28" alt="Formas de Pagamento"/></a>
                                        </div>

                                    </li>
                                </ul>
                                <ul class="menu-itens item-2">
                                    <li class="child">
                                        <a href="/seguranca" title="Auditado por Digicert" target="_blank"><img src="/media/selo-digicert.jpg" alt="Auditado por Digicert"/></a>
                                    </li>
                                    <li class="child">
                                        <a href="http://www.google.com.br/safebrowsing/diagnostic?site=http://www.talcha.com.br" title="Auditado por Google Secure" target="_blank"><img src="/media/selo-google.jpg" alt="Auditado por Google Secure"/></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
            <div class="col-md-4">
                <div class="talcha-news">
                    <h3>Cadastre seu email<br/><span>e fique por dentro das novidades</span></h3>

                    <form method="post" action="/newsletter/subscriber/new/" id="newsletter-validate-detail">
                        <input type="text" name="email" title="Digite seu email" class="input-text required-entry validate-email" id="newsletter"/>
                        <button title="ok" class="button bt-news"><span><span>ok</span></span>
                        </button>
                    </form>

                </div>
                <div class="talcha-network">
                    <h3>#Talchá nas Redes Sociais</h3>
                    <ul>
                        <li class="ei-facebook"><a target="_blank" title="Facebook" href="https://www.facebook.com/pages/Talch%C3%A1/167688186575714"><i class="fa fa-facebook"></i></a></li>
                        <li class="ei-instagram"><a target="_blank" title="Instagram" href="http://www.instagram.com/atalcha"><i class="fa fa-instagram"></i></a></li>
                        <li class="ei-twitter"><a target="_blank" title="Twitter" href="https://twitter.com/sigaatalcha"><i class="fa fa-twitter"></i></a></li>
                        <li class="ei-youtube"><a target="_blank" title="Youtube" href="https://www.youtube.com/channel/UCOwyY7bZjrPdEmorYjRK2aQ"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p class="texto">
                        TXA Comercio de Bebidas LTDA. - Telefone (11) 3078-4945 - Email: atendimento@talcha.com.br
                        <br/> CNPJ: 10.602.193/0001-04 / Inscrição Estadual: 148.473.354.114 / NIRE: 35.222.979.835
                        <br/> Endereço: Avenida Higienópolis, 618 - Shopping Pátio Higienópolis - Loja 2012 - Higienópolis - São Paulo/SP - CEP: 01238-000
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="seloTemp">selo 1</div>
                        </div>
                        <div class="col-md-4">
                            <div class="seloTemp">selo 2</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>