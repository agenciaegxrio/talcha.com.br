<header>
    <div class="top">
        <div class="container">
            <p class="duvidasCheckout">Dúvidas para finalizar sua compra? Entre em contato pelo nossa <a href="/central-de-atendimento" title="Central de Atendimento">Central de Atendimento.</a></p>
            <div class="row">
                <div class="col-md-6">
                    <p class="welcome-msg logged-out">Seja bem-vindo! Faça seu <a title="Login" href="/login" rel="nofollow">login</a> ou <a title="Cadastre-se" href="/login" rel="nofollow">cadastre-se</a></p>
                    <ul class="social-media nav nav-pills">
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-6 no-pd-l">
                    <ul class="nav nav-pills pull-right menu-top">
                        <li class="minha-conta">
                            <a title="Minha Conta" href="/account/" rel="nofollow"><i class="fa fa-user"></i> Minha Conta</a>
                        </li>
                        <li class="meus-pedidos">
                            <a title="Meus Pedidos" href="/account/orders" rel="nofollow"><i class="fa fa-list-ul"></i> Meus Pedidos</a>
                        </li>
                        <li class="lista-desejos">
                            <a title="Lista de Desejos" href="/account/wishlist/" rel="nofollow"><i class="fa fa-star"></i> Lista de Desejos</a>
                        </li>
                        <li class="central-atendimento">
                            <a title="Central de Atendimento" href="/central-de-atendimento" rel="nofollow"><i class="fa fa-comments"></i> Central de Atendimento</a>
                        </li>
                    </ul>        
                </div>
            </div>
        </div>
    </div>
    <div class="botton">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                  <a href="/" title="Talchá" class="logo sprite">Talchá</a>  
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="search-wrapper">
                                <vtex.cmc:fullTextSearchBox />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="shopping-cart-wrapper">
                                <a href="/checkout/" class="shopping-cart" rel="nofollow" title="Carrinho de Compras">
                                Minhas Compras <br/><span class="cart-count">0</span> itens <i class="fa fa-caret-down"></i>
                                </a>
                                <vtex.cmc:miniCart/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<nav>
    <div class="nav-1">
        <div class="container">
            <ul class="nav nav-pills">
                <li class="first">
                    <a href="/novo-no-cha" class="level-top" title="Novo no Chá?">
                        Novo no Chá?
                    </a>
                    <div class="subMenu">
                        <ul class="level0">
                            <li class="level1 nav-1-1 tipos-de-chas-368 first" id="">
                                <a href="/novo-no-cha/tipos-de-chas" title="Tipos de Chás" class="">
                                    Tipos de Chás
                                </a>
                            </li>
                            <li class="level1 nav-1-2 preparando-meu-cha-369" id="">
                                <a href="/novo-no-cha/preparando-meu-cha" title="Preparando meu Chá">
                                    Preparando meu Chá
                                </a>
                            </li>
                            <li class="level1 nav-1-3 a-historia-dos-chas-370" id="">
                                <a href="/novo-no-cha/a-historia-dos-chas" title="A História dos Chás" class="">
                                    A História dos Chás
                                </a>
                            </li>
                            <li class="level1 nav-1-4 dicas-371 last" id="">
                                <a href="/novo-no-cha/dicas" title="Dicas">
                                    Dicas
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="/acessorios" class="level-top" title="Acessórios">
                        Acessórios
                    </a>
                    <div class="subMenu">
                        <ul class="level0"> 
                            <li class="level1 nav-2-1 bule-352 first" id="">
                                <a href="/acessorios/bule" title="Bule">
                                    Bule
                                </a>
                            </li>
                            <li class="level1 nav-2-2 xicara-353" id="">
                                <a href="/acessorios/xicara" title="Xícara">
                                    Xícara
                                </a>
                            </li>
                            <li class="level1 nav-2-3 caneca-354" id="">
                                <a href="/acessorios/caneca" title="Caneca">
                                    Caneca
                                </a>
                            </li>
                            <li class="level1 nav-2-4 infusor-355 active" id="">
                                <a href="/acessorios/infusor" title="Infusor">
                                    Infusor
                                </a>
                            </li>
                            <li class="level1 nav-2-5 copo-356" id="">
                                <a href="/acessorios/copo" title="Copo">
                                    Copo
                                </a>
                            </li>
                            <li class="level1 nav-2-6 chaleira-357" id="">
                                <a href="/acessorios/chaleira" title="Chaleira">
                                    Chaleira
                                </a>
                            </li>
                            <li class="level1 nav-2-7 garrafa-termica-358" id="">
                                <a href="/acessorios/garrafa-termica" title="Garrafa Térmica">
                                    Garrafa Térmica
                                </a>
                            </li>
                            <li class="level1 nav-2-8 medidor-359" id="">
                                <a href="/acessorios/medidor" title="Medidor">
                                    Medidor
                                </a>
                            </li>
                            <li class="level1 nav-2-9 rechaud-360" id="">
                                <a href="/acessorios/rechaud" title="Rechaud">
                                    Rechaud
                                </a>
                            </li>
                            <li class="level1 nav-2-10 timer-394" id="">
                                <a href="/acessorios/timer" title="Timer">
                                    Timer   
                                </a>    
                            </li>
                            <li class="level1 nav-2-11 jarra-393" id="">
                                <a href="/acessorios/jarra" title="Jarra">
                                    Jarra   
                                </a>    
                            </li>
                            <li class="level1 nav-2-12 outro-361" id="">
                                <a href="/acessorios/outro" title="Outro">
                                    Outro   
                                </a>    
                            </li>
                            <li class="level1 nav-2-13 colecao-dinastias-373" id="">
                                <a href="/acessorios/colecao-dinastias" title="Coleção Dinastias">
                                    Coleção Dinastias
                                </a>
                            </li>
                            <li class="level1 nav-2-14 linha-fred-392 last" id="">
                                <a href="/acessorios/fred" title="Linha Fred">
                                    Linha Fred
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="/kits" class="level-top" title="Kits">
                        Kits
                    </a>
                    <div class="subMenu">
                        <ul class="level0">
                            <li class="level1 nav-3-1 para-escritorio-404 first" id="">
                                <a href="/kits/para-escritorio" title="Para Escritório">
                                    Para Escritório
                                </a>
                            </li>
                            <li class="level1 nav-3-2 chas-362" id="">
                                <a href="/kits/chas-1" title="Chás">
                                    Chás
                                </a>
                            </li>
                            <li class="level1 nav-3-3 chas-acessorios-363" id="">
                                <a href="/kits/chas-acessorios" title="Chás + Acessórios">
                                    Chás + Acessórios
                                </a>
                            </li>
                            <li class="level1 nav-3-4 presente-de-casamento-364" id="">
                                <a href="/kits/presente-de-casamento" title="Presente de Casamento">
                                    Presente de Casamento
                                </a>
                            </li>
                            <li class="level1 nav-3-5 presente-de-cha-de-cozinha-365" id="">
                                <a href="/kits/presente-de-cha-de-cozinha" title="Presente de Chá de Cozinha">
                                    Presente de Chá de Cozinha
                                </a>
                            </li>
                            <li class="level1 nav-3-6 para-mulher-366" id="">
                                <a href="/kits/para-mulher" title="Para Mulher">
                                    Para Mulher
                                </a>
                            </li>
                            <li class="level1 nav-3-7 para-homem-367 last" id="">
                                <a href="/kits/para-homem" title="Para Homem">
                                    Para Homem
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="last">
                    <a href="/outlet" class="level-top" title="OUTLET!">
                        OUTLET!
                    </a>
                </li>
            </ul>  
        </div>
    </div>
    <div class="nav-2">
        <div class="container">
            <ul id="nav">
                <li class="chas">
                    <a title="Chás" class="level-top" href="/todos-os-chas">
                        Todos os Chás
                    </a>
                    <div class="subMenu">
                        <div class="navStaticBlock">
                            <div class="widget widget-static-block">
                                <div class="menuFiltroWrap">

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Tipo</span>
                                        <ul>
                                            <li><a href="/todos-os-chas/l/puro.html" title="Puro">Puro</a></li>
                                            <li><a href="/todos-os-chas/l/blends.html" title="Blends">Blends</a></li>
                                            <li><a href="/todos-os-chas/l/detox.html" title="Detox">Detox</a></li>
                                            <li><a href="/todos-os-chas/l/organico.html" title="Orgânico">Orgânico</a></li>
                                            <li><a href="/todos-os-chas/l/premium.html" title="Premium">Premium</a></li>
                                            <li><a href="/todos-os-chas/l/relaxante.html" title="Relaxante">Relaxante</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Sabor</span>
                                        <ul>
                                            <li><a href="/todos-os-chas/l/floral.html" title="Floral">Floral</a></li>
                                            <li><a href="/todos-os-chas/l/citrico.html" title="Cítrico">Cítrico</a></li>
                                            <li><a href="/todos-os-chas/l/doce.html" title="Doce">Doce</a></li>
                                            <li><a href="/todos-os-chas/l/forte.html" title="Forte">Forte</a></li>
                                            <li><a href="/todos-os-chas/l/frutado.html" title="Frutado">Frutado</a></li>
                                            <li><a href="/todos-os-chas/l/suave.html" title="Suave">Suave</a></li>
                                            <li><a href="/todos-os-chas/l/mentolado.html" title="Mentolado">Mentolado</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Nível de Cafeína</span>
                                        <ul>
                                            <li><a href="/todos-os-chas/l/alto.html" title="Alto">Alto</a></li>
                                            <li><a href="/todos-os-chas/l/medio.html" title="Médio">Médio</a></li>
                                            <li><a href="/todos-os-chas/l/baixo.html" title="Baixo">Baixo</a></li>
                                            <li><a href="/todos-os-chas/l/sem-cafeina.html" title="Sem Cafeína">Sem Cafeína</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="menuProduto"><span class="menuTitulo">Destaque em Todos os Chás</span>
                                    <div class="produto-destaque-menu">
                                        <a href="http://www.talcha.com.br/rooibos-blueberry-17379.html" title="Rooibos Blueberry" class="product-image"><img src="http://www.talcha.com.br/media/catalog/product/cache/1/small_image/160x160/5e06319eda06f020e43594a9c230972dr-o-rooibos-blueberry-rooibos-blueberry-21-jpg" width="160" height="160" alt="Rooibos Blueberry"/></a>
                                        <div class="produto-conteudo-menu">
                                            <h2 class="product-name"><a href="http://www.talcha.com.br/rooibos-blueberry-17379.html" title="Rooibos Blueberry">Rooibos Blueberry</a></h2>
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:100%"></div>
                                                </div>
                                                <span class="amount">1 Comentário(s)</span>
                                            </div>
                                            <div class="price-box">
                                                <span class="label" id="configurable-price-from-17379"><span class="configurable-price-from-label">A partir</span></span>
                                                <p class="old-price">
                                                    <span class="price-label">De</span>
                                                    <span class="price" id="old-price-17379">R$68,00</span>
                                                </p>

                                                <p class="special-price">
                                                    <span class="price-label">Por</span>
                                                    <span class="price" id="product-price-17379">R$34,00</span>
                                                </p>
                                            </div>
                                            <button type="button" title="Adicionar ao carrinho" class="button e-icone cor-firme botao-menu-comprar" onclick="window.location='http://www.talcha.com.br/rooibos-blueberry-17379.html';">
                                                <span class="button-wrap">
                                        <span class="icon-wrap"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="button-title">Adicionar ao Carrinho</span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="cha-branco">
                    <a title="Chá Branco" class="level-top" href="/cha-branco">
                        <span class="icon"></span> Chá Branco
                    </a>
                    <div class="subMenu">
                        <div class="navStaticBlock">
                            <div class="widget widget-static-block">
                                <div class="menuFiltroWrap">
                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Tipo</span>
                                        <ul>
                                            <li><a href="/cha-branco/l/puro.html" title="Puro">Puro</a></li>
                                            <li><a href="/cha-branco/l/blends.html" title="Blends">Blends</a></li>
                                            <li><a href="/cha-branco/l/premium.html" title="Premium">Premium</a></li>
                                        </ul>
                                    </div>
                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Sabor</span>
                                        <ul>
                                            <li><a href="/cha-branco/l/frutado.html" title="Frutado">Frutado</a></li>
                                            <li><a href="/cha-branco/l/suave.html" title="Suave">Suave</a></li>
                                        </ul>
                                    </div>
                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Nível de Cafeína</span>
                                        <ul>
                                            <li><a href="/cha-branco/l/medio.html" title="Médio">Médio</a></li>
                                            <li><a href="/cha-branco/l/baixo.html" title="Baixo">Baixo</a></li>
                                            <li><a href="/cha-branco/l/sem-cafeina.html" title="Sem Cafeína">Sem Cafeína</a></li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="menuProduto"><span class="menuTitulo">Destaque em Chá Branco</span>
                                    <div class="produto-destaque-menu">
                                        <a href="http://www.talcha.com.br/neve-de-fujian-17366.html" title="Neve de Fujian" class="product-image"><img src="http://www.talcha.com.br/media/catalog/product/cache/1/small_image/160x160/5e06319eda06f020e43594a9c230972dn-e-neve-de-fujian-neve-de-fujian-21-jpg" width="160" height="160" alt="Neve de Fujian"/></a>
                                        <div class="produto-conteudo-menu">
                                            <h2 class="product-name"><a href="http://www.talcha.com.br/neve-de-fujian-17366.html" title="Neve de Fujian">Neve de Fujian</a></h2>

                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:%"></div>
                                                </div>
                                                <span class="amount">0 Comentário(s)</span>
                                            </div>

                                            <div class="price-box">



                                                <span class="label" id="configurable-price-from-17366"><span class="configurable-price-from-label">A partir</span></span> <span class="regular-price" id="product-price-17366">
                                Por <span class="price">R$44,00</span> </span>




                                            </div>

                                            <button type="button" title="Adicionar ao carrinho" class="button e-icone cor-firme botao-menu-comprar" onclick="window.location='http://www.talcha.com.br/neve-de-fujian-17366.html';">
                                                <span class="button-wrap">
                                        <span class="icon-wrap"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="button-title">Adicionar ao Carrinho</span>
                                                </span>
                                            </button>


                                        </div>





                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </li>
                <li class="cha-verde">
                    <a title="Chá Verde" class="level-top" href="/cha-verde">
                        <span class="icon"></span> Chá Verde
                    </a>
                    <div class="subMenu">

                        <div class="navStaticBlock">
                            <div class="widget widget-static-block">
                                <div class="menuFiltroWrap">

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Tipo</span>
                                        <ul>
                                            <li><a href="/cha-verde/l/puro.html" title="Puro">Puro</a></li>
                                            <li><a href="/cha-verde/l/blends.html" title="Blends">Blends</a></li>
                                            <li><a href="/cha-verde/l/detox.html" title="Detox">Detox</a></li>
                                            <li><a href="/cha-verde/l/organico.html" title="Orgânico">Orgânico</a></li>
                                            <li><a href="/cha-verde/l/premium.html" title="Premium">Premium</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Sabor</span>
                                        <ul>
                                            <li><a href="/cha-verde/l/floral.html" title="Floral">Floral</a></li>
                                            <li><a href="/cha-verde/l/citrico.html" title="Cítrico">Cítrico</a></li>
                                            <li><a href="/cha-verde/l/doce.html" title="Doce">Doce</a></li>
                                            <li><a href="/cha-verde/l/forte.html" title="Forte">Forte</a></li>
                                            <li><a href="/cha-verde/l/suave.html" title="Suave">Suave</a></li>
                                            <li><a href="/cha-verde/l/mentolado.html" title="Mentolado">Mentolado</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Nível de Cafeína</span>
                                        <ul>
                                            <li><a href="/cha-verde/l/alto.html" title="Alto">Alto</a></li>
                                            <li><a href="/cha-verde/l/medio.html" title="Médio">Médio</a></li>
                                        </ul>
                                    </div>

                                </div>


                                <div class="menuProduto"><span class="menuTitulo">Destaque em Chá Verde</span>

                                    <div class="produto-destaque-menu">





                                        <a href="http://www.talcha.com.br/matcha-premium-talcha.html" title="Matcha Premium Talchá" class="product-image"><img src="http://www.talcha.com.br/media/catalog/product/cache/1/small_image/160x160/5e06319eda06f020e43594a9c230972dm-a-matcha-premium-matcha-premium-talcha-21-jpeg" width="160" height="160" alt="Matcha Premium Talchá"/></a>

                                        <div class="produto-conteudo-menu">
                                            <h2 class="product-name"><a href="http://www.talcha.com.br/matcha-premium-talcha.html" title="Matcha Premium Talchá">Matcha Premium Talchá</a></h2>

                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:%"></div>
                                                </div>
                                                <span class="amount">0 Comentário(s)</span>
                                            </div>

                                            <div class="price-box">



                                                <span class="regular-price" id="product-price-18213">
                                Por <span class="price">R$279,90</span> </span>




                                                <p class="preco-parcelado">em <strong>2x</strong> de <strong class="price">R$139,95</strong> sem juros</p>
                                            </div>

                                            <button type="button" title="Adicionar ao carrinho" class="button e-icone cor-firme botao-menu-comprar" onclick="window.location='http://www.talcha.com.br/matcha-premium-talcha.html';">
                                                <span class="button-wrap">
                                        <span class="icon-wrap"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="button-title">Adicionar ao Carrinho</span>
                                                </span>
                                            </button>


                                        </div>





                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </li>
                <li class="cha-oolong">
                    <a title="Chá Oolong" class="level-top" href="/cha-oolong">
                        <span class="icon"></span> Chá Oolong
                    </a>
                    <div class="subMenu">
                        <div class="navStaticBlock">
                            <div class="widget widget-static-block">
                                <div class="menuFiltroWrap">

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Tipo</span>
                                        <ul>
                                            <li><a href="/cha-oolong/l/puro.html" title="Puro">Puro</a></li>
                                            <li><a href="/cha-oolong/l/blends.html" title="Blends">Blends</a></li>
                                            <li><a href="/cha-oolong/l/premium.html" title="Premium">Premium</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Sabor</span>
                                        <ul>
                                            <li><a href="/cha-oolong/l/citrico.html" title="Cítrico">Cítrico</a></li>
                                            <li><a href="/cha-oolong/l/suave.html" title="Suave">Suave</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Nível de Cafeína</span>
                                        <ul>
                                            <li><a href="/cha-oolong/l/medio.html" title="Médio">Médio</a></li>
                                        </ul>
                                    </div>

                                </div>


                                <div class="menuProduto"><span class="menuTitulo">Destaques em Chá Oolong</span>

                                    <div class="produto-destaque-menu">





                                        <a href="http://www.talcha.com.br/dung-ti-oolong.html" title="Dung Ti Oolong Premium" class="product-image"><img src="http://www.talcha.com.br/media/catalog/product/cache/1/small_image/160x160/5e06319eda06f020e43594a9c230972dd-u-dung-ti-oolong-premiun-dung-ti-oolong-premium-21-jpg" width="160" height="160" alt="Dung Ti Oolong Premium"/></a>

                                        <div class="produto-conteudo-menu">
                                            <h2 class="product-name"><a href="http://www.talcha.com.br/dung-ti-oolong.html" title="Dung Ti Oolong Premium">Dung Ti Oolong Premium</a></h2>

                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:0%"></div>
                                                </div>
                                                <span class="amount">0 Comentário(s)</span>
                                            </div>

                                            <div class="price-box">



                                                <span class="label" id="configurable-price-from-17303"><span class="configurable-price-from-label">A partir</span></span> <span class="regular-price" id="product-price-17303">
                                Por <span class="price">R$99,00</span> </span>




                                            </div>

                                            <button type="button" title="Adicionar ao carrinho" class="button e-icone cor-firme botao-menu-comprar" onclick="window.location='http://www.talcha.com.br/dung-ti-oolong.html';">
                                                <span class="button-wrap">
                                        <span class="icon-wrap"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="button-title">Adicionar ao Carrinho</span>
                                                </span>
                                            </button>


                                        </div>





                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </li>
                <li class="cha-preto">
                    <a title="Chá Preto" class="level-top" href="/cha-preto">
                        <span class="icon"></span> Chá Preto
                    </a>
                    <div class="subMenu">
                        <div class="navStaticBlock">
                            <div class="widget widget-static-block">
                                <div class="menuFiltroWrap">

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Tipo</span>
                                        <ul>
                                            <li><a href="/cha-preto/l/puro.html" title="Puro">Puro</a></li>
                                            <li><a href="/cha-preto/l/blends.html" title="Blends">Blends</a></li>
                                            <li><a href="/cha-preto/l/detox.html" title="Detox">Detox</a></li>
                                            <li><a href="/cha-preto/l/organico.html" title="Orgânico">Orgânico</a></li>
                                            <li><a href="/cha-preto/l/premium.html" title="Premium">Premium</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Sabor</span>
                                        <ul>
                                            <li><a href="/cha-preto/l/amadeirado.html" title="Amadeirado">Amadeirado</a></li>
                                            <li><a href="/cha-preto/l/forte.html" title="Forte">Forte</a></li>
                                            <li><a href="/cha-preto/l/frutado.html" title="Frutado">Frutado</a></li>
                                            <li><a href="/cha-preto/l/suave.html" title="Suave">Suave</a></li>
                                            <li><a href="/cha-preto/l/mentolado.html" title="Mentolado">Mentolado</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Nível de Cafeína</span>
                                        <ul>
                                            <li><a href="/cha-preto/l/alto.html" title="Alto">Alto</a></li>
                                        </ul>
                                    </div>

                                </div>


                                <div class="menuProduto"><span class="menuTitulo">Destaques em Chá Preto</span>

                                    <div class="produto-destaque-menu">





                                        <a href="http://www.talcha.com.br/earl-grey.html" title="Earl Grey " class="product-image"><img src="http://www.talcha.com.br/media/catalog/product/cache/1/small_image/160x160/5e06319eda06f020e43594a9c230972de-a-earl-grey-earl-grey-21-jpg" width="160" height="160" alt="Earl Grey "/></a>

                                        <div class="produto-conteudo-menu">
                                            <h2 class="product-name"><a href="http://www.talcha.com.br/earl-grey.html" title="Earl Grey ">Earl Grey </a></h2>

                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:100%"></div>
                                                </div>
                                                <span class="amount">1 Comentário(s)</span>
                                            </div>

                                            <div class="price-box">



                                                <span class="label" id="configurable-price-from-17576"><span class="configurable-price-from-label">A partir</span></span> <span class="regular-price" id="product-price-17576">
                                Por <span class="price">R$39,00</span> </span>




                                            </div>

                                            <button type="button" title="Adicionar ao carrinho" class="button e-icone cor-firme botao-menu-comprar" onclick="window.location='http://www.talcha.com.br/earl-grey.html';">
                                                <span class="button-wrap">
                                        <span class="icon-wrap"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="button-title">Adicionar ao Carrinho</span>
                                                </span>
                                            </button>


                                        </div>





                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </li>
                <li class="mate">
                    <a title="Mate" class="level-top" href="/mate">
                        <span class="icon"></span> Mate
                    </a>
                    <div class="subMenu">
                        <div class="navStaticBlock">
                            <div class="widget widget-static-block">
                                <div class="menuFiltroWrap">

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Tipo</span>
                                        <ul>
                                            <li><a href="/erva-mate/l/puro.html" title="Puro">Puro</a></li>
                                            <li><a href="/erva-mate/l/blends.html" title="Blends">Blends</a></li>
                                            <li><a href="/erva-mate/l/detox.html" title="Detox">Detox</a></li>
                                            <li><a href="/erva-mate/l/organico.html" title="Orgânico">Orgânico</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Sabor</span>
                                        <ul>
                                            <li><a href="/erva-mate/l/citrico.html" title="Cítrico">Cítrico</a></li>
                                            <li><a href="/erva-mate/l/forte.html" title="Forte">Forte</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Nível de Cafeína</span>
                                        <ul>
                                            <li><a href="/erva-mate/l/medio.html" title="Médio">Médio</a></li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="menuProduto"><span class="menuTitulo">Destaques em Erva Mate</span>

                                    <div class="produto-destaque-menu">





                                        <a href="http://www.talcha.com.br/mate-gengibre-citrus-17368.html" title="Mate Gengibre Citrus" class="product-image"><img src="http://www.talcha.com.br/media/catalog/product/cache/1/small_image/160x160/5e06319eda06f020e43594a9c230972dm-a-mate-gengibre-citrus-mate-gengibre-citrus-21-jpg" width="160" height="160" alt="Mate Gengibre Citrus"/></a>

                                        <div class="produto-conteudo-menu">
                                            <h2 class="product-name"><a href="http://www.talcha.com.br/mate-gengibre-citrus-17368.html" title="Mate Gengibre Citrus">Mate Gengibre Citrus</a></h2>

                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:%"></div>
                                                </div>
                                                <span class="amount">0 Comentário(s)</span>
                                            </div>

                                            <div class="price-box">



                                                <span class="label" id="configurable-price-from-17368"><span class="configurable-price-from-label">A partir</span></span> <span class="regular-price" id="product-price-17368">
                                Por <span class="price">R$26,00</span> </span>




                                            </div>

                                            <button type="button" title="Adicionar ao carrinho" class="button e-icone cor-firme botao-menu-comprar" onclick="window.location='http://www.talcha.com.br/mate-gengibre-citrus-17368.html';">
                                                <span class="button-wrap">
                                        <span class="icon-wrap"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="button-title">Adicionar ao Carrinho</span>
                                                </span>
                                            </button>


                                        </div>





                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </li>
                <li class="infusoes">
                    <a title="Infusões" class="level-top" href="/infusoes">
                        <span class="icon"></span> Infusões
                    </a>
                    <div class="subMenu">
                        <div class="navStaticBlock">
                            <div class="widget widget-static-block">
                                <div class="menuFiltroWrap">

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Tipo</span>
                                        <ul>
                                            <li><a href="/infusoes/l/puro.html" title="Puro">Puro</a></li>
                                            <li><a href="/infusoes/l/blends.html" title="Blends">Blends</a></li>
                                            <li><a href="/infusoes/l/detox.html" title="Detox">Detox</a></li>
                                            <li><a href="/infusoes/l/organico.html" title="Orgânico">Orgânico</a></li>
                                            <li><a href="/infusoes/l/relaxante.html" title="Relaxante">Relaxante</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Sabor</span>
                                        <ul>
                                            <li><a href="/infusoes/l/amadeirado.html" title="Amadeirado">Amadeirado</a></li>
                                            <li><a href="/infusoes/l/citrico.html" title="Cítrico">Cítrico</a></li>
                                            <li><a href="/infusoes/l/floral.html" title="Floral">Floral</a></li>
                                            <li><a href="/infusoes/l/forte.html" title="Forte">Forte</a></li>
                                            <li><a href="/infusoes/l/frutado.html" title="Frutado">Frutado</a></li>
                                            <li><a href="/infusoes/l/suave.html" title="Suave">Suave</a></li>
                                            <li><a href="/infusoes/l/mentolado.html" title="Mentolado">Mentolado</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Nível de Cafeína</span>
                                        <ul>
                                            <li><a href="/infusoes/l/sem-cafeina.html" title="Sem Cafeína">Sem Cafeína</a></li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="menuProduto"><span class="menuTitulo">Destaques em Infusões</span>

                                    <div class="produto-destaque-menu">





                                        <a href="http://www.talcha.com.br/vermelho-intenso.html" title="Vermelho Intenso" class="product-image"><img src="http://www.talcha.com.br/media/catalog/product/cache/1/small_image/160x160/5e06319eda06f020e43594a9c230972dv-e-vermelho-intenso-1-vermelho-intenso-21-jpg" width="160" height="160" alt="Vermelho Intenso"/></a>

                                        <div class="produto-conteudo-menu">
                                            <h2 class="product-name"><a href="http://www.talcha.com.br/vermelho-intenso.html" title="Vermelho Intenso">Vermelho Intenso</a></h2>

                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:100%"></div>
                                                </div>
                                                <span class="amount">1 Comentário(s)</span>
                                            </div>

                                            <div class="price-box">



                                                <span class="label" id="configurable-price-from-17843"><span class="configurable-price-from-label">A partir</span></span> <span class="regular-price" id="product-price-17843">
                                Por <span class="price">R$49,90</span> </span>




                                            </div>

                                            <button type="button" title="Adicionar ao carrinho" class="button e-icone cor-firme botao-menu-comprar" onclick="window.location='http://www.talcha.com.br/vermelho-intenso.html';">
                                                <span class="button-wrap">
                                        <span class="icon-wrap"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="button-title">Adicionar ao Carrinho</span>
                                                </span>
                                            </button>


                                        </div>





                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </li>
                <li class="rooibos-amp-honeybush">
                    <a title="Rooibos  Honeybush" class="level-top" href="/rooibos-honeybush">
                        <span class="icon"></span> Rooibos  Honeybush
                    </a>
                    <div class="subMenu">
                        <div class="navStaticBlock">
                            <div class="widget widget-static-block">
                                <div class="menuFiltroWrap">

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Tipo</span>
                                        <ul>
                                            <li><a href="/rooibos-honeybush/l/puro.html" title="Puro">Puro</a></li>
                                            <li><a href="/rooibos-honeybush/l/blends.html" title="Blends">Blends</a></li>
                                            <li><a href="/rooibos-honeybush/l/organico.html" title="Orgânico">Orgânico</a></li>
                                            <li><a href="/rooibos-honeybush/l/relaxante.html" title="Relaxante">Relaxante</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Sabor</span>
                                        <ul>
                                            <li><a href="/rooibos-honeybush/l/amadeirado.html" title="Amadeirado">Amadeirado</a></li>
                                            <li><a href="/rooibos-honeybush/l/floral.html" title="Floral">Floral</a></li>
                                            <li><a href="/rooibos-honeybush/l/citrico.html" title="Cítrico">Cítrico</a></li>
                                            <li><a href="/rooibos-honeybush/l/doce.html" title="Doce">Doce</a></li>
                                            <li><a href="/rooibos-honeybush/l/frutado.html" title="Frutado">Frutado</a></li>
                                            <li><a href="/rooibos-honeybush/l/suave.html" title="Suave">Suave</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Nível de Cafeína</span>
                                        <ul>
                                            <li><a href="/rooibos-honeybush/l/sem-cafeina.html" title="Sem Cafeína">Sem Cafeína</a></li>
                                        </ul>
                                    </div>

                                </div>


                                <div class="menuProduto"><span class="menuTitulo">Destaques em Rooibos  HoneyBush</span>

                                    <div class="produto-destaque-menu">





                                        <a href="http://www.talcha.com.br/ed-limitada-rooibos-paixao-18211.html" title="Edição Limitada  - Rooibos Thai" class="product-image"><img src="http://www.talcha.com.br/media/catalog/product/cache/1/small_image/160x160/5e06319eda06f020e43594a9c230972dr-o-rooibos-paixao-1-edicao-limitada-rooibos-thai-21-jpg" width="160" height="160" alt="Edição Limitada  - Rooibos Thai"/></a>

                                        <div class="produto-conteudo-menu">
                                            <h2 class="product-name"><a href="http://www.talcha.com.br/ed-limitada-rooibos-paixao-18211.html" title="Edição Limitada  - Rooibos Thai">Edição Limitada  - Rooibos Thai</a></h2>

                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:%"></div>
                                                </div>
                                                <span class="amount">0 Comentário(s)</span>
                                            </div>

                                            <div class="price-box">



                                                <span class="label" id="configurable-price-from-18211"><span class="configurable-price-from-label">A partir</span></span> <span class="regular-price" id="product-price-18211">
                                Por <span class="price">R$49,00</span> </span>




                                            </div>

                                            <button type="button" title="Adicionar ao carrinho" class="button e-icone cor-firme botao-menu-comprar" onclick="window.location='http://www.talcha.com.br/ed-limitada-rooibos-paixao-18211.html';">
                                                <span class="button-wrap">
                                        <span class="icon-wrap"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="button-title">Adicionar ao Carrinho</span>
                                                </span>
                                            </button>


                                        </div>





                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </li>
                <li class="flowering-teas">
                    <a title="Flowering Teas " class="level-top" href="/flowering-teas">
                        <span class="icon"></span> Flowering Teas
                    </a>
                    <div class="subMenu">
                        <div class="navStaticBlock">
                            <div class="widget widget-static-block">
                                <div class="menuFiltroWrap">

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Tipo</span>
                                        <ul>
                                            <li><a href="/flowering-teas/l/blends.html" title="Blends">Blends</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Sabor</span>
                                        <ul>
                                            <li><a href="/flowering-teas/l/floral.html" title="Floral">Floral</a></li>
                                            <li><a href="/flowering-teas/l/suave.html" title="Suave">Suave</a></li>
                                        </ul>
                                    </div>

                                    <div class="menuFiltro">
                                        <span class="menuTitulo">Compre por Nível de Cafeína</span>
                                        <ul>
                                            <li><a href="/flowering-teas/l/medio.html" title="Médio">Médio</a></li>
                                        </ul>
                                    </div>

                                </div>


                                <div class="menuProduto"><span class="menuTitulo">Destaques em Flowering Teas</span>

                                    <div class="produto-destaque-menu">





                                        <a href="http://www.talcha.com.br/agata.html" title="Agata" class="product-image"><img src="http://www.talcha.com.br/media/catalog/product/cache/1/small_image/160x160/5e06319eda06f020e43594a9c230972da-b-abat-grch-460-agatas-1-agata-21-jpg" width="160" height="160" alt="Agata"/></a>

                                        <div class="produto-conteudo-menu">
                                            <h2 class="product-name"><a href="http://www.talcha.com.br/agata.html" title="Agata">Agata</a></h2>

                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:%"></div>
                                                </div>
                                                <span class="amount">0 Comentário(s)</span>
                                            </div>

                                            <div class="price-box">



                                                <span class="regular-price" id="product-price-18253">
                                Por <span class="price">R$17,00</span> </span>
                                            </div>
                                            <button type="button" title="Adicionar ao carrinho" class="button e-icone cor-firme botao-menu-comprar" onclick="window.location='http://www.talcha.com.br/agata.html';">
                                                <span class="button-wrap">
                                        <span class="icon-wrap"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="button-title">Adicionar ao Carrinho</span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>