<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:vtex="http://www.vtex.com.br/2009/vtex-common" xmlns:vtex.cmc="http://www.vtex.com.br/2009/vtex-commerce" lang="en-us">

<head>
    <title>Talchá</title>
    <vtex:metaTags/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <vtex:template id="commonScripts" />
</head>
<body class="home">
            
    <!--  SubTemplate: Header -->
    <vtex:template id="Header" />
    <!-- /SubTemplate: Header -->
                    
    <section id="slide">
        
    </section>
    
    <section id="sobre-a-talcha">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3>
                        <span>Conheça mais sobre a talchá</span>
                    </h3>
                </div>
            </div>
            <div class="row">            
                <div class="col-md-4">
                    <a href="/institucional/empresa">
                        <img src="/arquivos/qwqq_r4_c3_s1-1399042971.png" alt="A Marca" />
                    </a>
                </div>
                <div class="col-md-4 alg-c">
                    <a href="/institucional/lojas">
                        <img src="/arquivos/qwqq_r4_c7_s1-1399043007.png" alt="Nossas Lojas" />
                    </a>
                </div>
                <div class="col-md-4 alg-r">
                    <a href="#">
                        <img src="/arquivos/qwqq_r4_c12_s1-1399043076.png" alt="O Chá" />
                    </a>
                </div>
            </div>
        </div>
    </section>
    
    <section id="nossas-recomendacoes">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3>
                        <span>Saiba mais sobre cada tipo de chá</span>
                    </h3>
                </div>
                <div class="lista-produto prateleira">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="saiba-mais">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3>
                        <span>Saiba mais sobre cada tipo de chá</span>
                    </h3>
                </div>
                <div class="col-md-4 vitrine-cha cha-preto">
                    <div class="title-cha">
                        <h3><a href="/cha-preto" title="Chá Preto">Chá Preto</a></h3>
                        <a href="/cha-preto" title="Ver Todos - Chá Preto" class="ver-todos"> ver todos</a>
                    </div>
                    <div class="container-cha">
                        <div class="image-cha">
                            <div class="widget widget-static-block">
                                <img src="/arquivos/cha-preto.jpg" alt="Chá Preto"/>
                            </div>
                        </div>
                        <div class="content-cha">
                            <div class="widget widget-static-block">
                                <p>O chá preto foi o primeiro a chegar no ocidente pelas mãos dos navegadores holandeses no século XII e por isso é um dos mais populares do mundo. Dependendo da região onde é produzido como Assam, Darjeeling ou China por exemplo, seu sabor pode ser pronunciado.</p>
                                <p><a href="/cha-preto" title="Saiba mais sobre chá preto">Saiba mais...</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="list-cha">
                        <div class="title-bar">
                            <span class="title-lista-cha">Top 3 Chá Preto</span>
                            <a href="/cha-preto" title="Ver Todos - Chá Preto">
                                ver todos
                            </a>
                        </div>
                        <div class="lista-produto-cha">
                            <ul class="produto-grid colunas-4">
                                <li class="produto first">
                                    <h2 class="nome-produto">
                                        <a href="http://www.talcha.com.br/cream-earl-grey.html" title="Cream Earl Grey">Cream Earl Grey
                                        </a>
                                    </h2>
                                    <div class="price-box">
                                        <span class="regular-price" id="product-price-17736">
                                    Por <span class="price">R$39,00</span> </span>
                                    </div>
                                </li>
                                <li class="produto">

                                    <h2 class="nome-produto"><a href="http://www.talcha.com.br/cha-do-amor.html" title="Chá do Amor">Chá do Amor</a></h2>
                                    <div class="price-box">
                                        <span class="regular-price" id="product-price-17731">
                                    Por <span class="price">R$34,00</span> </span>
                                    </div>
                                </li>
                                <li class="produto last">
                                    <h2 class="nome-produto"><a href="http://www.talcha.com.br/golden-pu-erh-reserva.html" title="Golden Pu-Erh Reserva">Golden Pu-Erh Reserva</a></h2>

                                    <div class="price-box">

                                        <span class="regular-price" id="product-price-17726">
                                    Por <span class="price">R$89,00</span> </span>





                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 vitrine-cha cha-verde">
                    <div class="title-cha">
                        <h3><a href="/cha-verde" title="Chá Verde">Chá Verde</a></h3>
                        <a href="/cha-verde" title="Ver Todos - Chá Verde" class="ver-todos"> ver todos</a>
                    </div>
                    <div class="container-cha">
                        <div class="image-cha">
                            <div class="widget widget-static-block">
                                <img src="/arquivos/cha-verde.jpg" alt="Chá Verde"/>
                            </div>
                        </div>
                        <div class="content-cha">
                            <div class="widget widget-static-block">
                                <p>Existem 2 métodos para produzir este chá: O Chinês e o Japonês. O sabor da infusão difere bastante. A bebida proveniente da infusão das folhas do chá verde chinês é mais amarelada com sabor floral enquanto que a infusão do chá verde japonês possui uma coloração verde...</p>
                                <p><a href="/cha-verde" title="Saiba mais sobre chá verde">Saiba mais...</a></p>
                            </div>

                        </div>
                    </div>
                    <div class="list-cha">
                        <div class="title-bar">
                            <span class="title-lista-cha">Top 3 Chá Verde</span>
                            <a href="/cha-verde" title="Ver Todos - Chá Verde"> ver todos</a>
                        </div>
                        <div class="lista-produto-cha">





                            <ul class="produto-grid colunas-4">
                                <li class="produto first">

                                    <h2 class="nome-produto"><a href="http://www.talcha.com.br/touareg.html" title="Touareg">Touareg</a></h2>



                                    <div class="price-box">



                                        <span class="regular-price" id="product-price-17570">
                                    Por <span class="price">R$29,00</span> </span>





                                    </div>

                                </li>




                                <li class="produto">

                                    <h2 class="nome-produto"><a href="http://www.talcha.com.br/matcha-premium-talcha.html" title="Matcha Premium Talchá">Matcha Premium Talchá</a></h2>



                                    <div class="price-box">



                                        <span class="regular-price" id="product-price-18213">
                                    Por <span class="price">R$279,90</span> </span>





                                    </div>

                                </li>




                                <li class="produto last">

                                    <h2 class="nome-produto"><a href="http://www.talcha.com.br/vigor.html" title="Vigor">Vigor</a></h2>



                                    <div class="price-box">



                                        
                                        
                                        <p class="special-price">
                                            <span class="price-label">Por</span>
                                            <span class="price" id="product-price-17863">
                                    R$34,00                </span>
                                        </p>





                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 vitrine-cha cha-oolong">
                    <div class="title-cha">
                        <h3><a href="/cha-oolong" title="Chá Oolong">Chá Oolong</a></h3>
                        <a href="/cha-oolong" title="Ver Todos - Chá Oolong" class="ver-todos"> ver todos</a>
                    </div>
                    <div class="container-cha">
                        <div class="image-cha">
                            <div class="widget widget-static-block">
                                <img src="/arquivos/cha-oolong.jpg" alt="Chá Oolong"/>
                            </div>

                        </div>
                        <div class="content-cha">
                            <div class="widget widget-static-block">
                                <p>Essa família de chás fica entre os verdes e os pretos, e também são chamados de chá azul. O seu processamento é muito variado(por conta do país que o produz, nesse caso China ou Taiwan) pois o nível de oxidação pode variar de 15 a 70%. Portanto ele pode estar mais...</p>
                                <p><a href="/cha-oolong" title="Saiba mais sobre chá oolong">Saiba mais...</a></p>
                            </div>

                        </div>
                    </div>
                    <div class="list-cha">
                        <div class="title-bar">
                            <span class="title-lista-cha">Top 3 Chá Oolong</span>
                            <a href="/cha-oolong" title="Ver Todos - Chá Oolong">ver todos</a>
                        </div>
                        <div class="lista-produto-cha">





                            <ul class="produto-grid colunas-4">
                                <li class="produto first">

                                    <h2 class="nome-produto"><a href="http://www.talcha.com.br/oolong-citrico-17367.html" title="Oolong Cítrico">Oolong Cítrico</a></h2>



                                    <div class="price-box">



                                        

                                        <p class="special-price">
                                            <span class="price-label">Por</span>
                                            <span class="price" id="product-price-17367">
                                    R$39,00                </span>
                                        </p>





                                    </div>

                                </li>




                                <li class="produto">

                                    <h2 class="nome-produto"><a href="http://www.talcha.com.br/dung-ti-oolong.html" title="Dung Ti Oolong Premium">Dung Ti Oolong Premium</a></h2>



                                    <div class="price-box">



                                        <span class="regular-price" id="product-price-17303">
                                    Por <span class="price">R$99,00</span> </span>





                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="conteudo-interativo">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3>
                        <span>Conteúdo Interativo</span>
                    </h3>
                </div>
                <div class="col-md-6">
                    <h4 class="alg-c">Aprenda como preparar o seu chá</h4>
                    <iframe width="510" height="315" src="//www.youtube.com/embed/tU8EASNh-Ts" frameborder="0" allowfullscreen=""></iframe>
                </div>
                <div class="col-md-6">
                    <h4 class="alg-c">Aprenda a fazer seu Flowering Tea</h4>
                    <iframe width="510" height="315" src="//www.youtube.com/embed/33v5Er48RsE" frameborder="0" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </section>
                    
  <!--  <div id="content" class="row-fluid">-->
  <!--      <div class="sidebar span2">-->
  <!--          <vtex.cmc:departmentNavigator/>-->
  <!--      </div>-->
		<!--<div id="collections" class="span10">-->
  <!--          <div class="giftlist-insert">-->
  <!--             <vtex:contentPlaceHolder id="bannersegmentado" />-->
  <!--          </div>-->
		<!--	<div class="collectionWrap">-->
  <!--              <vtex:contentPlaceHolder id="Destaques" />-->
  <!--          </div>-->
            
  <!--          <div class="collectionWrap">-->
  <!--              <vtex:contentPlaceHolder id="Lançamentos" />   -->
  <!--          </div>-->
		<!--</div>-->
  <!--  </div>-->
    
    <!--  SubTemplate: Footer -->
    <vtex:template id="Footer" />
    <!-- /SubTemplate: Footer -->
    <script src="/arquivos/talcha-javascripts.min.js"></script>
</body>
</html>