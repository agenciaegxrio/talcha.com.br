<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:vtex="http://www.vtex.com.br/2009/vtex-common" xmlns:vtex.cmc="http://www.vtex.com.br/2009/vtex-commerce" lang="pt-br">

<head>
    <title>Talchá</title>
    <vtex:metaTags/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <vtex:template id="commonScripts" />
</head>
<body>
            
    <!--  SubTemplate: Header -->
    <vtex:template id="Header" />
    <!-- /SubTemplate: Header -->
                
    <section id="category">
        <div class="container">
            <div id="topBanners">
                <!--  placeholder -->
                <vtex:contentPlaceHolder id="Banner-Principal" />
                <!-- /placeholder -->
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div id="departmentNavigator">
                        <!--  Controle: navegador de departamento -->
                        <vtex.cmc:searchNavigator />
                        <!-- /Controle: navegador de departamento -->
                    </div>                                      
                </div>
                <div class="col-sm-10">     
                                                            
                    <div class="title-category">
                        <!--  Controle: Titulo -->
                        <vtex.cmc:searchTitle />
                        <!-- /Controle: Titulo -->
                    </div>
                
                    <div id="collections">
                        <div class="giftlist-insert">
                            <vtex.cmc:GiftListInsertSkuV2/>
                        </div>
                        <div class="collectionWrap">
                            <!--  Controle: Resultado da busca / Lista de produtos -->
                            <vtex.cmc:searchResult layout="ef3fcb99-de72-4251-aa57-71fe5b6e149f" itemCount="12" columnCount="3" />
                            <!--  /Controle: Resultado da busca / Lista de produtos -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--  SubTemplate: Footer -->
    <vtex:template id="Footer" />
    <!-- /SubTemplate: Footer -->
    <script src="/arquivos/talcha-javascripts.min.js"></script>
</body>
</html>