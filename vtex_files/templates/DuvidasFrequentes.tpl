<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:vtex="http://www.vtex.com.br/2009/vtex-common" xmlns:vtex.cmc="http://www.vtex.com.br/2009/vtex-commerce">

<head>
    <title>Dúvidas frequentes - Talchá</title>
    <vtex:metaTags/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <vtex:template id="commonScripts" />
</head>

<body>

    <!--  SubTemplate: Header -->
    <vtex:template id="Header" />
    <!-- /SubTemplate: Header -->
    
    <!-- Dúvidas Frequentes -->
    <section id="duvidas-frequentes">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Início</a></li>
                <li><a href="#">Ajuda e Suporte</a></li>
                <li class="active">Dúvidas Frequentes</li>
            </ol>
            <div class="row">
                <div class="col-md-2">
                    <ul class="page-nav nav">
                        <li><a href="/institucional/empresa">Central de Atendimento</a></li>
                        <li><a href="/institucional/duvidas-frequentes">Dúvidas Frequentes</a></li>
                        <li><a href="/institucional/trocas-e-devolucoes">Trocas e Devoluções</a></li>
                        <li><a href="#">Minha Senha</a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <h1 class="page-title">
                        Dúvidas Frequentes
                    </h1>
                    <div class="page-content row">
                        <div class="col-md-12">
                            <p style="text-align: justify;">Nós nos esforçamos sempre para fazer um site simples e completo para que você navegue e realize suas compras da maneira mais fácil e com segurança. No entanto, se você tiver alguma dúvida ao navegar em nosso site, sobre um produto ou pedido realizado, por favor, entre em contato com a nossa Central de Atendimento através do telefone (11) 3078-4945, por chat ou e-mail. Nosso horário de atendimento é das 9h às 18h, nos dias úteis. Clique aqui para acessar a CENTRAL DE ATENDIMENTO. </p>
                            <p style="text-align: justify;">Preparamos algumas perguntas e respostas rápidas com as dúvidas mais frequentes que podem ajudar você:</p>
                            <h3 style="text-align: left;">Como fazer uma compra na Talchá?</h3>
                            <p style="text-align: justify;">Realizar uma compra em nosso site é simples! Nossos produtos estão divididos em categorias e subcategorias. Clicando nelas, você vai encontrar uma enorme variedade de opções e modelos. Para ver o produto com mais detalhes, basta clicar na foto. O produto aparecerá com a descrição completa e fotos de vários ângulos para melhor avaliação.</p>
                            <p style="text-align: justify;">Gostou? Então escolha - se houver - a cor e o tamanho desejado e clique na opção "Adicionar ao Carrinho". Se quiser apenas um produto, clique em "Finalizar Compra" na tela que se abrirá. Se quiser escolher mais produtos, basta continuar a navegação e ir adicionando os produtos desejados ao Carrinho de Compras, pois nele ficarão salvos todos os produtos escolhidos, que podem ser excluídos a qualquer momento. Ao optar por 'Finalizar Compra', aparecerá uma página com os seus pedidos e valores. Confira e continue. Siga as instruções até efetivar a compra. No final da última página “confirmação do pedido”, clique em “continuar” para efetuar o pagamento com cartão de crédito ou boleto bancário. Para mais informações sobre pagamentos clique em FORMAS DE PAGAMENTO.</p>
                            <h3 style="text-align: left;">Quais os dados que devem constar em meu cadastro?</h3>
                            <p style="text-align: justify;">Itens básicos como Nome Completo, E-mail, Endereço e CPF, de forma a garantir a compra e a correta entrega. Para evitar transtornos com o cartão de crédito, os dados do comprador que utilizará esta modalidade de pagamento devem ser exatamente os mesmos que constam no cartão de crédito. Além disso, certifique-se de preencher corretamente o endereço de entrega e CPF, pois estes dados serão utilizados para emissão da Nota Fiscal e fundamentais para a correta entrega do seu pedido.</p>
                            <h3 style="text-align: left;">Qual o valor do frete e o prazo de entrega?</h3>
                            <p style="text-align: justify;">O valor do frete e o prazo de entrega variam de acordo com a cidade, Estado e tipo de postagem. Para mais informações clique em PRAZOS E CUSTOS DE ENTREGA.</p>
                            <h3 style="text-align: left;">Posso trocar ou devolver um produto?</h3>
                            <p style="text-align: justify;">Sim. A primeira troca ou devolução é gratuita. Para saber mais clique em TROCAS E DEVOLUÇÕES.</p>
                            <h3 style="text-align: left;">Fechei meu pedido e quando voltei no site vi que o produto estava esgotado. O que ocorreu?</h3>
                            <p style="text-align: justify;">Não se preocupe! Quando você finaliza seu pedido, o site automaticamente retira o item do estoque. Se o produto era peça única, ele aparecerá como "Esgotado", para assegurar que nenhum outro cliente adquira um produto sem estoque.</p>
                            <h3 style="text-align: left;">O site funciona como um catálogo online da loja física?</h3>
                            <p style="text-align: justify;">Não. O site é uma loja virtual da Talchá pode ter produtos e preços diferenciados da loja física.</p>
                            <h3 style="text-align: left;">O preço do site é igual ao preço da loja física?</h3>
                            <p style="text-align: justify;">Não necessariamente. Em geral, a política de preços é semelhante. No entanto, podem haver preços especiais e promoções exclusivamente para o site ou para a loja física. A existência de preços diferenciados não caracterizará a obrigação da loja física em igualar o preço do site e vice-versa. A loja física e o site se reservam o direito de analisar eventuais descontos especiais individualmente, sem caracterizar em obrigação. Em caso de dúvidas sobre preços de produtos, por favor, entre em contato com nossa Central de Atendimento ou com a nossa loja.</p>
                            <h3 style="text-align: left;">O site e a loja física possuem os mesmos produtos?</h3>
                            <p style="text-align: justify;">Não necessariamente. Em geral, o site e a loja física compartilham a mesma grade e estoque de produtos. No entanto, por se tratarem de unidades distintas, bem como por políticas de fornecedores específicos, podem haver produtos vendidos exclusivamente no site ou na loja física. A existência de um produto na loja física não caracterizará a obrigação do site em vendê-lo e vice-versa. Em caso de dúvidas sobre produtos, por favor, entre em contato com nossa Central de Atendimento ou com a nossa loja.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Dúvidas Frequentes -->
    
    <!--  SubTemplate: Footer -->
    <vtex:template id="Footer" />
    <!-- /SubTemplate: Footer -->
    
    <script src="/arquivos/talcha-javascripts.min.js"></script>
</body>
</html>