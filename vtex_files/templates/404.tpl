<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:vtex="http://www.vtex.com.br/2009/vtex-common" xmlns:vtex.cmc="http://www.vtex.com.br/2009/vtex-commerce" lang="en-us">

<head>
    <title>Nenhum resultado encontrado - Talchá</title>
    <vtex:metaTags/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <vtex:template id="commonScripts" />
</head>
<body class="home">
            
    <!--  SubTemplate: Header -->
    <vtex:template id="Header" />
    <!-- /SubTemplate: Header -->
     
    <section id="404">
        <div class="container">
            <h3>A busca não retornou resultados</h3>
        </div>
    </section>               
    <!--  SubTemplate: Footer -->
    <vtex:template id="Footer" />
    <!-- /SubTemplate: Footer -->
    <script src="/arquivos/talcha-javascripts.min.js"></script>
</body>
</html>