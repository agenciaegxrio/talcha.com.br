<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:vtex="http://www.vtex.com.br/2009/vtex-common" xmlns:vtex.cmc="http://www.vtex.com.br/2009/vtex-commerce" lang="pt-br">

<head>
    <title>Talchá</title>
    <vtex:metaTags/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <vtex:template id="commonScripts" />
</head>
<body class="product">
            
    <!--  SubTemplate: Header -->
    <vtex:template id="Header" />
    <!-- /SubTemplate: Header -->
                
    <section id="product">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Início</a></li>
                <li><a href="#">Chá preto</a></li>
                <li class="active">Grand yu</li>
            </ol>
            <div class="row">
                <div class="col-md-6">
                    <vtex.cmc:ProductImage />
                </div>
                <div class="col-md-6">
                    <div class="well">
                        <div>
                            <!--  Component: Product Name -->
                            <h1 class="lead"><vtex.cmc:productName /></h1>
                            <!--<vtex.cmc:stockKeepingUnitSelection />-->

                            <!--  Component: Short description -->
                            <vtex.cmc:productDescriptionShort />
                        </div>
                    
                        <!--  Component: Buy button / Notify me -->
                        <div id="BuyButton" class="pull-right span4">
                            <vtex.cmc:BuyButton />
                            <vtex.cmc:GiftListInsertSkuV2 popup="true" />
                        </div>
                        

                        <div class="seletorSku clearfix">
                            <!--  Component: SKU selector -->
                            <vtex.cmc:stockKeepingUnitSelection />
                        </div>
                    
                        <!--  Component: Sellers -->
                        <vtex.cmc:SellerOptions />
                
                        <!--  Component: Price -->
                        <vtex.cmc:skuPrice />
                    
                        <!--  Component: Sales channels -->
                        <vtex.cmc:SalesChannelDropList />
                    
                        <!--  Component: Addicional services -->
                        <vtex.cmc:stockKeepingUnitService />
                        
                        <!--  Component: Payment methods -->
                        <vtex.cmc:OtherPaymentMethod/>                
            
                        <!--Acessórios-->
                        <div id="boxConsumidoresCompraramCompraram" class="clearfix">
                            <vtex:contentPlaceHolder id="Acessorios" />
                        </div>
                        <!--Acessórios-->
                        
                        <!--Sugestões-->
                        <div id="boxConsumidoresCompraramCompraram" class="clearfix">
                            <vtex:contentPlaceHolder id="Sugestoes" />
                        </div>
                        <!--Sugestões-->
                    
                        <!--Similares-->
                        <div id="boxConsumidoresCompraramCompraram" class="clearfix">
                            <vtex:contentPlaceHolder id="Similares" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="product-description">
                        <h2 class="title">Descrição do Produto</h2>
                        <!--  Component: Description -->
                        <vtex.cmc:ProductDescription />
                    </div>
        
                    <!--  Component: Specification -->
                    <vtex.cmc:productSpecification />
                    
                    <!--  Component: Cross selling -->
                    <vtex.cmc:BuyTogether/>
                
                    <div class="vitrine">
                        <!--  Placeholder: Recomentations -->
                        <vtex:contentPlaceHolder id="PHrecomendamos" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--  SubTemplate: Footer -->
    <vtex:template id="Footer" />
    <!-- /SubTemplate: Footer -->
    <script src="/arquivos/talcha-javascripts.min.js"></script>
</body>
</html>