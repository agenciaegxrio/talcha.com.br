define(function(require, exports, module) {
  "use strict";

  var app = require("app"),
      jQuery = window.jQuery;

  var Category =  {
    events: function() {

      var self = this;

      // captura o clique no h5 do titulo da categoria para possibilitar ocultar ou exibir com o clique
 	
      console.log("add events");

      jQuery('#departmentNavigator .menu-departamento > h5').on('click', function(ev){
      	ev.preventDefault();

      	var next = $(this).next();

      	next.toggleClass('hide');

      });


    }
  };

  module.exports = Category;
    
});