define(function(require, exports, module) {
  "use strict";
  
  // require("jquery-private");
  // require("jquery.migrate");
  // require("bootstrap.collapse");
  var App = {},
      home = require("home"),
      category = require("category");

  home.events();
  category.events();
 
  return App;

});