jQuery(function(){
  // jQuery(window).load(function() {  
    if(jQuery('body').hasClass('home')){
      jQuery('.flexslider').flexslider({
        animation: "slide",
        prevText: "", 
        nextText: "",
        initDelay: 0
      });
    }
  // });
  
  // aguarda o dom ficar pronto para aplicar os eventos
  jQuery(window).load(function(){
    
    // adicionando placeholder no campo de busca no header
    jQuery('input.fulltext-search-box')
      .attr('value','')
      .attr('placeholder','Digite o que procura...');

    // captura o clique no h5 do titulo da categoria para possibilitar ocultar ou exibir com o clique
    jQuery('#departmentNavigator .menu-departamento > h5').on('click', function(ev){
      ev.preventDefault();
      var self = $(this);

      self.next().slideToggle( "slow", function() {
        self.toggleClass('expand');
      });

    });

  }); 
});