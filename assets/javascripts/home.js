define(function(require, exports, module) {
  "use strict";

  var app = require("app"),
      jQuery = window.jQuery;

  require("flexslider");

  var Home =  {
    events: function() {

      var self = this;

      self.slider();

    },
    slider: function(){
      jQuery(window).load(function() {
        
        if(jQuery('body').hasClass('home')){
          jQuery('.flexslider').flexslider({
            animation: "slide",
            prevText: "", 
            nextText: "",
            initDelay: 0
          });
        }
      });
    }
  };

  module.exports =  Home;
    
});